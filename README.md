# nullfx.crc

[![CI Status](http://img.shields.io/travis/nullfx/swift-nullfx.crc.svg?style=flat)](https://travis-ci.org/nullfx/swift-nullfx.crc)
[![Version](https://img.shields.io/cocoapods/v/nullfx.crc.svg?style=flat)](http://cocoapods.org/pods/nullfx.crc)
[![License](https://img.shields.io/cocoapods/l/nullfx.crc.svg?style=flat)](http://cocoapods.org/pods/nullfx.crc)
[![Platform](https://img.shields.io/cocoapods/p/nullfx.crc.svg?style=flat)](http://cocoapods.org/pods/nullfx.crc)


## Installation

nullfx.crc is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'nullfx.crc'
```


## License

nullfx.crc is available under the MIT license. See the LICENSE file for more info.
